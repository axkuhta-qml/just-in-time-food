import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    signal done();
    id: root

    visible: false

    Column {
        anchors.centerIn: parent
        spacing: 50

        Column {
            spacing: 10

            Text {
                text: "Welcome to Just in Time Food"
                font.bold: true
            }
            Text {
                text: "Здесь вы можете заказать еду"
            }
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter

            contentItem: Row {
                padding: 5
                Text {
                    text: "Продолжить"
                }
            }

            background: Rectangle {
                color: "#F5F5F5"
                radius: 2
            }

            onClicked: root.done()
        }
    }
}
