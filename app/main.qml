import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15


Window {
    width: 320
    height: 480
    visible: true
    title: qsTr("Hello World")

    StackView {
        id: stack
        anchors.fill: parent
        initialItem: intro_screen
    }

    IntroScreen {
        id: intro_screen

        onDone: {
            stack.push(map_screen)
        }
    }

    MapScreen {
        id: map_screen

        onDone: function(title) {
            store_screen.location_name = title
            store_screen.selection = fetch_sample_data() // Здесь по идее должно быть получение меню этого конкетного ресторана

            store_screen.update_item_list()

            stack.push(store_screen)
        }

        onBack: {
            stack.pop()
        }
    }

    StoreScreen {
        id: store_screen

        onDone: function(location_name, order) {
            cart_screen.location_name = location_name
            cart_screen.selection = fetch_sample_data()
            cart_screen.order = order

            cart_screen.update_item_list()
            cart_screen.compute_order_price()

            stack.push(cart_screen)
        }

        onBack: {
            stack.pop()
        }
    }

    CartScreen {
        id: cart_screen

        onBack: {
            store_screen.update_item_list()
            store_screen.compute_order_price()
            stack.pop()
        }

        onDone: {
            stack.push(final_screen)
        }
    }

    FinalScreen {
        id: final_screen
    }

    function fetch_sample_data() {
        var elements = [
            {
                image: "pics/43909076_e1364665e7_q.jpg",
                title: "Картошка фри",
                price_s: 85,
                price_m: 105,
                price_l: 125,
                type_s: "Маленькая",
                type_m: "Средняя",
                type_l: "Большая",
                sizes: true,
                section: "Картошка"
            },
            {
                image: "pics/11540628696_06337ea621_q.jpg",
                title: "Наггетсы",
                price_s: 119,
                price_m: 139,
                price_l: 159,
                type_s: "8 штук",
                type_m: "12 штук",
                type_l: "16 штук",
                sizes: true,
                section: "Мясо"
            },
            {
                image: "pics/49927836631_ace1fbf831_q.jpg",
                title: "Мясо",
                price: 99,
                sizes: false,
                section: "Мясо"
            },
            {
                image: "pics/49799453933_80533acd66_q.jpg",
                title: "Салат с рисом",
                price: 99,
                sizes: false,
                section: "Салаты"
            },
            {
                image: "pics/49853032732_5886b216a0_q.jpg",
                title: "Набор рыбы",
                price: 239,
                sizes: false,
                section: "Салаты"
            },
            {
                image: "pics/49931039698_f4e87a9515_q.jpg",
                title: "Набор роллов",
                price: 239,
                sizes: false,
                section: "Салаты"
            },
            {
                image: "pics/49931860612_e69b49ab9f_q.jpg",
                title: "Салат с рыбой",
                price: 179,
                sizes: false,
                section: "Салаты"
            }
        ]

        return elements
    }
}
