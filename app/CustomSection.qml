import QtQuick 2.15

Column {
    property alias text: section_title.text

    bottomPadding: 5
    topPadding: 5
    spacing: 5

    Text {
        id: section_title
        font.pixelSize: 20
        color: "#202020"
    }

    Rectangle {
        width: parent.parent.width
        height: 1
        color: "#202020"
    }
}
