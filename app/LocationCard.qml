import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    property alias image: card_image.source
    property alias title: title.text
    property alias hint: hint.text
    signal clicked()

    width: ListView.view.width
    height: 90
    id: root

    Button {
        anchors.fill: parent

        onClicked: root.clicked()

        background: Rectangle {
            color: "#F5F5F5"
        }

        ColumnLayout {
            anchors.right: card_image.right
            anchors.left: parent.left
            Text {
                id: title

                padding: 10
                font.pixelSize: 20
                text: "Название"
            }
            Text {
                id: hint

                padding: 10
                text: "Адрес"
            }
        }
        Image {
            id: card_image

            width: 90
            height: 90
            anchors.right: parent.right
        }
    }
}
