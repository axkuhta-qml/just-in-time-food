import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Page {
    signal done();
    id: root

    visible: false

    Column {
        anchors.centerIn: parent
        spacing: 50

        ColumnLayout {
            spacing: 10

            Text {
                Layout.alignment: Qt.AlignHCenter
                text: "Заказ оформлен!"
                font.bold: true
            }
            Text {
                Layout.alignment: Qt.AlignHCenter
                text: "Номер заказа:"
            }
            Text {
                Layout.alignment: Qt.AlignHCenter
                text: "1234"
                font.bold: true
                font.pixelSize: 28
            }
            Text {
                Layout.alignment: Qt.AlignHCenter
                text: "Приходите как можно скорее."
            }
        }
    }
}
