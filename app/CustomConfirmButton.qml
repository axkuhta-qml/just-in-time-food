import QtQuick 2.15
import QtQuick.Controls 2.15

Button {
    text: "Подтвердить"

    icon.source: "icons/done.svg"

    background: Rectangle {
        border.color: enabled ? "limegreen" : "#AAAAAA"
        color: down ? "limegreen" : "#F5F5F5"
        radius: 2
    }
}
