import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    property alias image: card_image.source
    property alias title: title.text
    property alias size_s: size_s
    property alias size_m: size_m
    property alias size_l: size_l
    signal input()

    property alias count: item_count_selector.count
    property int price_s: 10
    property int price_m: 20
    property int price_l: 30
    property int price: 1
    property string type: size_s.text

    property bool size_selector_enabled: false

    id: root

    height: card_contents.implicitHeight + 5 + 5
    width: ListView.view.width

    Rectangle {
        anchors.fill: parent

        border.color: "black"
        radius: 5

        ColumnLayout {
            id: card_contents
            anchors.fill: parent
            anchors.margins: 5
            spacing: 5

            RowLayout {
                spacing: 10

                Image {
                    id: card_image

                    sourceSize.height: 90
                    sourceSize.width: 90
                }

                ColumnLayout {
                    spacing: 10

                    Text {
                        id: title

                        font.pixelSize: 20
                        text: "Название"
                    }

                    ItemCountSelector {
                        id: item_count_selector
                        base_price: root.price
                        onInput: root.input()
                    }
                }
            }

            RowLayout {
                id: size_selector

                Layout.fillWidth: true

                visible: root.size_selector_enabled
                CustomRadioButton {
                    Layout.fillWidth: true

                    id: size_s

                    text: "Small"

                    checked: root.type === text

                    onClicked: {
                        root.price = root.price_s
                        root.type = text
                        root.input()
                    }
                }
                CustomRadioButton {
                    Layout.fillWidth: true

                    id: size_m

                    text: "Medium"

                    checked: root.type === text

                    onClicked: {
                        root.price = root.price_m
                        root.type = text
                        root.input()
                    }
                }
                CustomRadioButton {
                    Layout.fillWidth: true

                    id: size_l

                    text: "Large"

                    checked: root.type === text

                    onClicked: {
                        root.price = root.price_l
                        root.type = text
                        root.input()
                    }
                }
            }
        }
    }
}
