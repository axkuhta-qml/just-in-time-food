import QtQuick 2.15
import QtQuick.Controls 2.15

Button {
    id: control

    implicitWidth: 25
    implicitHeight: 25

    // https://stackoverflow.com/questions/40823503/qml-button-change-text-color
    contentItem: Text {
        text: control.text
        font: control.font

        color: control.down ? "white" : "black"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        color: control.down ? "black" : "white"
        border.color: "black"
        radius: 2
    }
}
