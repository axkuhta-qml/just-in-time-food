import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQml.Models 2.15

Page {
    property alias item_list: item_list

    property string location_name: "Location missing"
    property var selection: ({})
    property var order: ({})
    property string order_price: "0 руб"
    signal done(var location_name, var order)
    signal back()

    id: root
    visible: false

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        TopPanel {
            text: "Выбор еды"
            onBack: root.back()
        }

        Rectangle {
            id: storefront

            Layout.fillWidth: true
            Layout.fillHeight: true

            ListView {
                anchors.fill: parent
                id: item_list

                // Margin
                anchors.leftMargin: 5
                anchors.rightMargin: 5

                // Padding
                topMargin: 5
                bottomMargin: 5

                spacing: 5
                clip: true

                model: ListModel {

                }

                delegate: StoreItemCard {
                    image: model.image
                    title: model.title
                    price: model.price
                    size_s.text: model.type_s
                    size_m.text: model.type_m
                    size_l.text: model.type_l
                    price_s: model.price_s
                    price_m: model.price_m
                    price_l: model.price_l
                    size_selector_enabled: sizes
                    type: model.type
                    count: model.count
                    onInput: root.handle_input(this)
                }

                section {
                    property: "section"
                    criteria: ViewSection.FullString

                    // Section does not support spacing
                    // Bottom margin added manually in CustomSection
                    delegate: CustomSection {
                        text: section
                    }
                }
            }
        }

        Rectangle {
            id: bottom_panel
            color: "#F5F5F5"
            Layout.fillWidth: true
            height: 80

            RowLayout {
                anchors.fill: parent
                anchors.margins: 5

                Button {
                    contentItem: ColumnLayout {
                        Text {
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            text: "Стоимость\nзаказа:"
                        }
                        Text {
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                            text: root.order_price
                        }
                    }
                    background: Item {
                        // No background
                    }
                }

                Item {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }

                CustomConfirmButton {
                    Layout.fillHeight: true
                    text: "Продолжить"

                    id: confirm_button

                    enabled: false

                    onClicked: root.done(root.location_name, root.order)
                }
            }
        }
    }

    function update_item_list() {
        item_list.model.clear()

        for (var el of selection) {
            if (el.title in order) {
                el.count = order[el.title].count
                el.type = order[el.title].type
                el.price = order[el.title].price
            } else {
                el.count = 0
                el.type = el.type_s ?? ""
                el.price = el.price_s ?? el.price
            }

            item_list.model.append(el)
        }
    }

    function compute_order_price() {
        var total = 0
        var uniq = 0

        for (var title in order) {
            var item = order[title]

            total += item.count * item.price
            uniq++
        }

        confirm_button.enabled = (uniq > 0)
        order_price = total + " руб"
    }

    function handle_input(context) {
        order[context.title] = {
            count: context.count,
            price: context.price,
            type: context.type
        }

        for (var title in order) {
            var item = order[title]

            if (item.count === 0) {
                delete order[title]
            }
        }

        compute_order_price()

        console.log(JSON.stringify(order, null, 2))
    }
}
