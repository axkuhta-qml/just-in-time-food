import QtQuick 2.15
import QtQuick.Layouts 1.15

ColumnLayout {
    property int base_price: 0
    property int count: 0
    signal input()

    function increment() {
        count++
        input()
    }

    function decrement() {
        count--
        input()
    }

    id: root

    RowLayout {
        visible: count == 0

        CustomRockerButton {
            implicitWidth: 60
            onClicked: root.increment()
            text: root.base_price + "Р"
        }
    }

    RowLayout {
        visible: count > 0

        spacing: 5

        CustomRockerButton {
            onClicked: root.increment()
            text: "+"
        }

        Text {
            text: root.count
        }

        CustomRockerButton {
            onClicked: root.decrement()
            text: "-"
        }

        Item {
            width: 10
        }

        Text {
            text: root.count * root.base_price + "Р"
        }
    }
}
