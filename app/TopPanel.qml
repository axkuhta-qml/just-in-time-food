import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Rectangle {
    property alias text: panel_text.text
    signal back()

    id: root
    color: "#F5F5F5"
    Layout.fillWidth: true
    height: 40

    Button {
        icon.source: "icons/back.svg"
        implicitWidth: 40

        background: Item {

        }

        onClicked: root.back()
    }

    Text {
        id: panel_text
        text: "Screen title"
        anchors.centerIn: parent
    }
}
