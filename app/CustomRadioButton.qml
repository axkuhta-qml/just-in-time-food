import QtQuick 2.15
import QtQuick.Controls 2.15

RadioButton {
    id: control

    indicator: null

    // Текст обернут в Item поскольку надо спрятать implicitWidth от кнопки, иначе она делает глупые вещи внутри RowLayout
    contentItem: Item {
        implicitHeight: 20

        Text {
            text: control.text
            font: control.font

            anchors.centerIn: parent

            color: control.checked ? "white" : "black"
        }
    }

    background: Rectangle {
        border.color: "black"
        radius: 2

        color: control.checked ? "black" : "white"
    }
}
