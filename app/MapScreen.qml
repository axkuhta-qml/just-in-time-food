import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtWebEngine 1.10

Page {
    signal done(var title)
    signal back()

    id: root
    visible: false

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        TopPanel {
            text: "Выбор ресторана"
            onBack: root.back()
        }

        Rectangle {
            id: map

            Layout.fillWidth: true
            Layout.fillHeight: true

            WebEngineView {
                id: browser

                // https://switch2osm.org/using-tiles/getting-started-with-leaflet/
                anchors.fill: parent
                url: `data:text/html,
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
                <style>
                    html, body {
                        height: 100%;
                        padding: 0;
                        margin: 0;
                    }

                    .map {
                        width: 100%;
                        height: 100%;
                    }
                </style>
                <div id="map" class="map"></div>
                <script type="text/javascript">
                    // initialize Leaflet
                    var map = L.map('map').setView({lon: 0, lat: 0}, 2);

                    // add the OpenStreetMap tiles
                    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                      maxZoom: 19,
                      attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
                    }).addTo(map);

                    // show the scale bar on the lower left corner
                    L.control.scale({imperial: false, metric: true}).addTo(map);

                    // show a marker on the map
                    L.marker({lon: 0, lat: 0}).bindPopup('The center of the world').addTo(map);
                </script>`
            }

            Button {
                text: "Dump state"
                anchors.centerIn: parent

                onClicked: {
                    var result_fn = function(result) {
                        console.log(JSON.stringify(result, null, 2))
                    }

                    browser.runJavaScript("L", result_fn)
                }
            }
        }

        Rectangle {
            id: bottom_panel

            Layout.fillWidth: true
            height: 40

            color: "black"
        }
    }

    Rectangle {
        property bool expanded: false

        id: restaurant_list

        width: parent.width
        anchors.bottom: parent.bottom

        states: [
            State {
                name: "expanded"
                PropertyChanges {
                    target: restaurant_list
                    height: 400
                }
            },
            State {
                name: "collapsed"
                PropertyChanges {
                    target: restaurant_list
                    height: bottom_panel.height
                }
            }
        ]

        transitions: [
            Transition {
                PropertyAnimation {
                    properties: "height"
                    easing.type: Easing.InOutQuad
                    duration: 250
                }
            }
        ]

        state: expanded ? "expanded" : "collapsed"

        Button {
            id: restaurant_list_button

            anchors.top: parent.top
            width: parent.width
            height: bottom_panel.height
            text: parent.expanded ? "Показать карту" : "Показать все рестораны"
            icon.source: parent.expanded ? "icons/collapse.svg" : "icons/expand.svg"

            background: Rectangle {
                color: "#F5F5F5"
            }

            onClicked: {
                parent.expanded = !parent.expanded
            }
        }

        ListView {
            anchors.top: restaurant_list_button.bottom
            width: restaurant_list.width
            height: parent.height - restaurant_list_button.height
            topMargin: 5
            spacing: 5
            clip: true

            id: item_list

            model: ListModel {
                ListElement {
                    image: "pics/14683277223_00d052eb2d_q.jpg"
                    title: "МТЦ Новый"
                    hint: "ул. Советская, 51/1"
                }
                ListElement {
                    image: "pics/16797085430_77fc8b844c_q.jpg"
                    title: "ТК Литвинова"
                    hint: "ул. Литвинова, 17"
                }
                ListElement {
                    image: "pics/3799334631_3dc7a8eb99_q.jpg"
                    title: "ТРЦ MegaHome"
                    hint: "ул. Сергеева, 3/2"
                }
                ListElement {
                    image: "pics/40277954712_10d5e6121a_q.jpg"
                    title: "ТЦ Версаль"
                    hint: "ул. Академическая, 31"
                }
                ListElement {
                    image: "pics/50579482397_c1a150c724_q.jpg"
                    title: "ТЦ Электрон"
                    hint: "ул. Рабочая, 18Г"
                }
                ListElement {
                    image: "pics/51684039164_901a74ddbc_q.jpg"
                    title: "на Советской"
                    hint: "ул. Советская, 73"
                }
            }

            delegate: LocationCard {
                image: model.image
                title: model.title
                hint: model.hint
                onClicked: root.done(this.title)
            }
        }
    }
}
